#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Array of Controls"
msgstr "Steuerelement-Array"

#: .project:2
msgid "Example how to use an array of controls"
msgstr "Beispiel, wie man einen Array von Steuerelementen benutzt"

#: FMain.class:8
msgid "Quit"
msgstr "Beenden"

#: FMain.class:45
msgid "You have clicked the button &1"
msgstr "Du hast die Taste &1 angeklickt"

#: FMain.class:70
msgid "Clear"
msgstr "Löschen"

#: FMain.class:78
msgid "There is no number I could dial."
msgstr "Es gibt keine Nummer zum Wählen."

#: FMain.class:80
msgid "Dialing "
msgstr "Wähle "

#: FMain.form:25
msgid "Dial"
msgstr "Wählen"
